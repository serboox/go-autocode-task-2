package main

import (
	"testing"
)

type squareTestCase struct {
	start     Point
	a         uint
	end       Point
	perimeter uint
	area      uint
}

var squareTestCases = []squareTestCase{
	{Point{1, 1}, 5, Point{6, -4}, 20, 25},
	{Point{1, 1}, 6, Point{7, -5}, 24, 36},
	{Point{1, 1}, 7, Point{8, -6}, 28, 49},
	{Point{0, 0}, 7, Point{7, -7}, 28, 49},
}

func TestEnd(t *testing.T) {
	for _, testCase := range squareTestCases {
		square := &Square{
			start: testCase.start,
			a:     testCase.a,
		}
		endPoint := square.End()
		if testCase.end.x != endPoint.x ||
			testCase.end.y != endPoint.y {
			t.Fatal(
				"For", testCase,
				"expected", testCase.end,
				"got", endPoint,
			)
		}
	}
}

func TestPerimeter(t *testing.T) {
	for _, testCase := range squareTestCases {
		square := &Square{
			start: testCase.start,
			a:     testCase.a,
		}
		perimeter := square.Perimeter()
		if testCase.perimeter != perimeter {
			t.Fatal(
				"For", testCase,
				"expected", testCase.perimeter,
				"got", perimeter,
			)
		}
	}
}

func TestArea(t *testing.T) {
	for _, testCase := range squareTestCases {
		square := &Square{
			start: testCase.start,
			a:     testCase.a,
		}
		area := square.Area()
		if testCase.area != area {
			t.Fatal(
				"For", testCase,
				"expected", testCase.area,
				"got", area,
			)
		}
	}
}
