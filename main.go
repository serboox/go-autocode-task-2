package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y int
}

type Square struct {
	start Point
	a     uint
}

func main() {
	s := Square{Point{1, 1}, 5}
	fmt.Println(s.End())
	fmt.Println(s.Perimeter())
	fmt.Println(s.Area())
}

func (s *Square) End() *Point {
	return &Point{
		x: s.start.x + int(s.a),
		y: s.start.y - int(s.a),
	}
}

func (s *Square) Perimeter() uint {
	return s.a * 4
}

func (s *Square) Area() uint {
	return uint(math.Pow(float64(s.a), 2))
}
